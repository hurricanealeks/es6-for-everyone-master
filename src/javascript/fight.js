export function fight(firstFighter, secondFighter) {
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    while (firstFighterHealth > 0 && secondFighterHealth > 0) {
        firstFighterHealth = firstFighterHealth - getDamage(secondFighter, firstFighter);
        if (firstFighterHealth <= 0) break;
        secondFighterHealth = secondFighterHealth - getDamage(firstFighter, secondFighter);
        if (secondFighterHealth <= 0) break;
    }
    return firstFighterHealth > 0 ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
    let damage = getHitPower(attacker) - getBlockPower(enemy);
    return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
    let chance = Math.random() + 1;
    return fighter.attack * chance;
}

export function getBlockPower(fighter) {
    let chance = Math.random() + 1;
    return fighter.defense * chance;
}
