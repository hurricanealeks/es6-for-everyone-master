import {createElement} from '../helpers/domHelper';
import {showModal} from './modal';

export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({title, bodyElement});
}

function createFighterDetails(fighter) {
    const {name, attack, defense, health, source} = fighter;
    const attributes = { src: source };
    const fighterDetails = createElement({tagName: 'div', className: 'modal-body'});
    const nameElement = createElement({tagName: 'span', className: 'fighter-name'});
    const attackElement = createElement({tagName: 'span', className: 'fighter-attack'});
    const defenseElement = createElement({tagName: 'span', className: 'fighter-defense'});
    const healthElement = createElement({tagName: 'span', className: 'fighter-health'});
    const imgElement = createElement({tagName: 'span', className: 'fighter-image', attributes});
    nameElement.innerText = "Name: " + name;
    healthElement.innerText = "Health: " + health;
    attackElement.innerText = "Attack: " + attack;
    defenseElement.innerText = "Defense: " + defense;
    fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, imgElement);

    return fighterDetails;
}
