import {createElement} from "../helpers/domHelper";
import {showModal} from "./modal";

export function showWinnerModal(fighter) {
    const {name, source} = fighter;
    const title = 'Winner is ';
    const attributes = { src: source };
    const winnerElement = createElement({tagName: 'div', className: 'modal-body'});
    const nameElement = createElement({tagName: 'span', className: 'fighter-name'});
    const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes});
    nameElement.innerText = name;
    winnerElement.appendChild(nameElement, imageElement);
    showModal({title, winnerElement});
}